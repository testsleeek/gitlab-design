class exports.Constants
	@sidebarWidth: 240
	@rowHeight: 35
	@selectedBorderWidth: 3
	@rowFontSize: 14
	@indentation: 16
	@breadcrumbsItemsPadding: 17
	@breadcrumbsTitlePadding: 3
	@breadcrumbsImageWidth: 19
	@dropdownWidth: 170

